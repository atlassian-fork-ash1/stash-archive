package it.com.atlassian.bitbucket.internal.archive.browser;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.atlassian.bitbucket.test.runners.FuncTestSplitterRunner;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;
import com.atlassian.webdriver.testing.annotation.WindowSize;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import org.junit.AfterClass;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;

import static com.atlassian.bitbucket.test.TestRuleOrderingSyntax.outerRules;

@RunWith(FuncTestSplitterRunner.class)
@WindowSize(width = 1024, height = 768)
public abstract class AbstractBrowserTest extends BaseRetryingFuncTest {

    private static final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @AfterClass
    public static void clearBrowserCookies() {
        BITBUCKET.getTester().getDriver().manage().deleteAllCookies();
    }

    @Override
    protected List<TestRule> getRetryRuleChain() {
        return outerRules(
                new WebDriverScreenshotRule(),
                new WindowSizeRule(),
                new SessionCleanupRule()
        ).around(super.getRetryRuleChain());
    }

    <P extends BitbucketPage> P loginAsAdmin(Class<P> page, Object... args) {
        return loginAs(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), page, args);
    }

    private <P extends BitbucketPage> P loginAs(String username, String password, Class<P> page, Object... args) {
        clearBrowserCookies();
        return BITBUCKET.visit(BitbucketLoginPage.class).login(username, password, page, args);
    }
}
