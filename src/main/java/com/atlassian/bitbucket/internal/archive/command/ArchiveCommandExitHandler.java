package com.atlassian.bitbucket.internal.archive.command;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.NoSuchCommitException;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.InvalidRefNameException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandExitHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Locale;

import static java.util.Objects.requireNonNull;

/**
 * An {@link com.atlassian.bitbucket.scm.CommandExitHandler exit handler} for {@code git archive}, which handles
 * its stderr messages.
 *
 * @since 2.1
 */
public class ArchiveCommandExitHandler extends GitCommandExitHandler {

    private final String commitId;

    public ArchiveCommandExitHandler(@Nonnull I18nService i18nService, @Nullable Repository repository,
                                     @Nonnull String commitId) {
        super(i18nService, repository);

        this.commitId = requireNonNull(commitId, "commitId");
    }

    @Override
    protected void evaluateStdErr(String stdErr, String command) {
        //Normalize stdErr's case. Some versions of Git use "Not" and some use "not"
        String check = stdErr.toLowerCase(Locale.ROOT);

        //It's a little unfortunate that this is the output, but git archive doesn't require a _commit_; it
        //requires a tree-ish. That means it will accept a commit, and archive the root tree for the commit,
        //but it will also just accept a tree. Since it's unlikely most callers are _using_ that capability,
        //however, when we see this message we assume a commit was provided and throw accordingly
        if (check.startsWith("fatal: not a tree object")) {
            throw newNoSuchCommitException(commitId);
        }
        if (check.startsWith("fatal: not a valid object name")) {
            throw newInvalidRefNameException();
        }

        super.evaluateStdErr(stdErr, command);
    }

    /**
     * Creates <i>and throws</i> a new {@link NoSuchCommitException} for the specified {@code commitId}. If a
     * {@link Repository repository} was set in the constructor, its details will be included in the exception
     * message.
     * <p>
     * <b>Note</b>: This method <i>never</i> returns; it <i>always</i> throws the created exception.
     * <p>
     * <b>Version support</b>: This method was added to {@code GitCommandExitHandler} in Bitbucket Server 4.6. To
     * allow this plugin to continue working in 4.0-4.5, it's duplicated here. In 4.6+, this method will override
     * the Bitbucket Server-provided method with identical functionality to what it already has.
     *
     * @param commitId the {@link Commit#getId ID} of the commit that was not found
     * @return this call <i>never</i> returns; the return type is only to allow callers to {@code throw} the result
     *         of the call to help the Java compiler understand the calling method ends with this call
     * @throws NoSuchCommitException with the provided {@code commitId}
     * @since 2.1
     */
    protected NoSuchCommitException newNoSuchCommitException(String commitId) {
        if (repository != null) {
            throw new NoSuchCommitException(
                    i18nService.createKeyedMessage("bitbucket.service.repository.commitnotfound",
                            repository.getName(), commitId),
                    commitId);
        }
        throw new NoSuchCommitException(
                i18nService.createKeyedMessage("bitbucket.service.repository.commitnotfound.generic", commitId),
                commitId);
    }

    private InvalidRefNameException newInvalidRefNameException() {
        throw new InvalidRefNameException(
                i18nService.createKeyedMessage("bitbucket.archive.invalidref", commitId),
                commitId);
    }
}