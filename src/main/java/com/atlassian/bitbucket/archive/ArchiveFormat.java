package com.atlassian.bitbucket.archive;

import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

/**
 * Archive file formats supported by git-archive.
 */
public enum ArchiveFormat {

    TAR("tar"),
    TAR_GZ("tar.gz", "tgz"),
    ZIP("zip");

    private final String extension;
    private final Set<String> extensions;

    ArchiveFormat(String... extensions) {
        this.extensions = ImmutableSet.copyOf(extensions);

        extension = extensions[0];
    }

    /**
     * @param extension the extension to convert to a {@code ArchiveFormat}, which may not be {@code null}
     * @return the {@code ArchiveFormat} associated with the specified {@code extension}, which will
     *         never be {@code null}
     * @throws IllegalArgumentException if the provided {@code extension} doesn't match any format
     * @throws NullPointerException if the provided {@code extension} is {@code null}
     * @since 2.1
     */
    @Nonnull
    @SuppressWarnings("deprecation")
    public static ArchiveFormat fromExtension(@Nonnull String extension) {
        ArchiveFormat format = forExtension(requireNonNull(extension, "extension"));
        if (format == null) {
            throw new IllegalArgumentException("No ArchiveFormat is associated with extension [" + extension + "].");
        }

        return format;
    }

    /**
     * @param extension the extension to convert to an {@code ArchiveFormat}, which may be {@code null}
     * @return the {@code ArchiveFormat} associated with the specified {@code extension}, or {@code null} if the
     *         provided {@code extension} is {@code null} or is not associated with a format
     * @deprecated in 2.1 for removal in 3.0. Use {@link #fromExtension(String)} instead, and perform your own
     *             {@code null}-checking prior to calling the method.
     */
    @Deprecated
    @Nullable
    public static ArchiveFormat forExtension(@Nullable String extension) {
        if (extension == null) {
            return null;
        }
        String filterExtension = extension.toLowerCase(Locale.ROOT);

        return Stream.of(values())
                .filter(format -> format.getExtensions().contains(filterExtension))
                .findFirst()
                .orElse(null);
    }

    /**
     * @return the default extension for this format
     * @since 2.1
     */
    @Nonnull
    public String getDefaultExtension() {
        return extension;
    }

    /**
     * @return the default extension for this format
     * @deprecated in 2.1 for removal in 3.0. Use {@link #getDefaultExtension()} instead.
     */
    @Deprecated
    @Nonnull
    public String getExtension() {
        return extension;
    }

    /**
     * @return the set of all extensions associated with this format, which will always contain at least one value
     * @since 2.1
     */
    @Nonnull
    public Set<String> getExtensions() {
        return extensions;
    }
}
